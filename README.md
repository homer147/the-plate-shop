# The Plate Shop Version 1.0

## Installation

Requires node.js and was built using v6.10.2.

Also recommended to have gulp and karma installed globally. `npm install gulp karma -g`

Open terminal and navigate to the root of the repo, then download the required packages.

```npm install```

## Usage

After you have downloaded the packages, you can now build the css

```gulp build```

and then you can start the app

```npm start```

Using your browser, navigate to http://localhost:3000.

Unit tests can be run by typing

```gulp test```

After the tests have run for the first time, you can find a 'Coverage' folder in the root directory. This contains a HTML file that reports on unit test coverage.

## Other Comments

I have chosen to use Angular as a framework because I think the 2-way data binding, and the use of custom directives will be very useful in this exercise.

## End Comments

Unfortunately I have not been able to finish the exercise in time. I have made the home page and mini cart, but I do not have product pages or a cart page.

I wish I had been able to complete more of the exercise, but I enjoyed working on it all the same and I appreciate the opportunity to sit the test.

best regards
Michael

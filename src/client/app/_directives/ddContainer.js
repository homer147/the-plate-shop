(function () {
  'use strict';

  var app = angular.module('plateShop');

  app.directive('dropdownContainer', function () {
    return {
      restrict: 'C',
      link: function (scope, elem) {
        var onHover,
          offHover;

        onHover = function () {
          elem.addClass('hovering');
        };

        offHover = function () {
          elem.removeClass('hovering');
        };

        elem.bind('mouseover', onHover);
        elem.bind('mouseleave', offHover);

        scope.$on('$destroy', function () {
          elem.unbind('mouseover', onHover);
          elem.unbind('mouseleave', offHover);
        });
      }
    };
  });
}());

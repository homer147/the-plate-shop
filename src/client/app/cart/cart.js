(function () {
  'use strict';

  var app = angular.module('plateShop');

  app.controller('cartCtrl', ['$scope', 'cartService',
    function ($scope, cartService) {
      $scope.cartIds = cartService.getCart();

      $scope.cart = [];

      function updateCartTotal() {
        $scope.totalPrice = 0;
        for (var i = 0; i < $scope.cart.length; i++) {
          $scope.totalPrice += $scope.cart[i].totalPrice;
        }
      }

      function updateCart() {
        $scope.cart = [];
        angular.forEach($scope.cartIds, function (value, key) {
          if (key !== 'length') {
            $scope.cart.push(value);
          }
        });

        updateCartTotal();
      }

      $scope.removeFromCart = function (item) {
        cartService.removeFromCart(item, true);
      };

      updateCart();
      cartService.registerCartObservers(updateCart);
    }]);

  app.directive('miniCart', function () {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/app/cart/mini-cart.html',
      controller: 'cartCtrl'
    };
  });
}());

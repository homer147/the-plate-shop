/* jslint node: true */
/* global inject, expect, jasmine, getJSONFixture */

(function () {
  'use strict';

  describe('cartCtrl - Cart Controller', function () {
    var createController,
      scope,
      cartCtrl;

    beforeEach(function () {
      module('plateShop');
    });

    beforeEach(inject(function ($rootScope, $controller) {

      createController = function () {
        scope = $rootScope.$new();

        return $controller('cartCtrl', {
          $scope: scope
        });
      };

      cartCtrl = createController();
    }));

    it('should set default variables', function () {
      expect(scope.cart).toEqual([]);
    });
  });
}());

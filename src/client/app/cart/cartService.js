(function () {
  'use strict';

  var app = angular.module('plateShop');

  app.service('cartService', function () {
    var cart = { length: 0 },
      cartObserverCallbacks = [],
      getCart,
      addToCart,
      removeFromCart,
      registerCartObservers,
      notifyCartObservers,
      emptyCart;

    getCart = function () {
      return cart;
    };

    addToCart = function (newItem) {
      if (cart.hasOwnProperty(newItem.title)) {
        cart[newItem.title].total++;
        cart[newItem.title].totalPrice = cart[newItem.title].total * cart[newItem.title].item.price;
      } else {
        cart[newItem.title] = {};
        cart[newItem.title].total = 1;
        cart[newItem.title].totalPrice = newItem.price;
        cart[newItem.title].item = newItem;
        cart.length++;
      }

      notifyCartObservers();
    };

    removeFromCart = function (removeItem, removeAll) {
      if (cart.hasOwnProperty(removeItem) && removeAll) {
        delete cart[removeItem];
        cart.length--;
        notifyCartObservers();
      }

      if (cart.hasOwnProperty(removeItem) && cart[removeItem].total === 1) {
        delete cart[removeItem];
        cart.length--;
        notifyCartObservers();
      }

      if (cart.hasOwnProperty(removeItem) && cart[removeItem].total > 1) {
        cart[removeItem].total--;
        cart[removeItem].totalPrice = cart[removeItem].total * cart[removeItem].item.price;
        notifyCartObservers();
      }
    };

    emptyCart = function () {
      cart = {};
      cart.length = 0;
      notifyCartObservers();
    };

    registerCartObservers = function (callback) {
      cartObserverCallbacks.push(callback);
    };

    notifyCartObservers = function () {
      angular.forEach(cartObserverCallbacks, function (callback) {
        callback();
      });
    };

    return {
      getCart: getCart,
      addToCart: addToCart,
      removeFromCart: removeFromCart,
      emptyCart: emptyCart,
      registerCartObservers: registerCartObservers
    };
  });
}());

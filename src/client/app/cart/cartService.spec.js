/* jslint node: true */
/* global inject, expect, jasmine, getJSONFixture */

(function () {
  'use strict';

  describe('cartService - Service tracking the cart items', function () {
    var cartService,
      result,
      title,
      title1,
      httpBackend,
      mockProducts;

    beforeEach(function () {
      module('plateShop');
    });

    beforeEach(inject(function (_cartService_, _$httpBackend_) {
      cartService = _cartService_;
      httpBackend = _$httpBackend_;
      jasmine.getJSONFixtures().fixturesPath = 'base/src/client/app/cart';
      mockProducts = getJSONFixture('cartService.spec.json');
    }));

    it('should be defined and the cart should be empty', function () {
      expect(cartService).toBeDefined();
      result = cartService.getCart();
      expect(result).toEqual({ 'length': 0 });
    });

    it('should add a new item to the cart, and increment it again', function () {

      // check cart is empty
      result = cartService.getCart();
      expect(result).toEqual({ 'length': 0 });

      // add first item
      cartService.addToCart(mockProducts[0]);
      title = mockProducts[0].title;
      result = cartService.getCart();
      expect(result.length).toEqual(1);
      expect(result[title]).toEqual(jasmine.any(Object));

      // add the same item again
      cartService.addToCart(mockProducts[0]);
      title = mockProducts[0].title;
      result = cartService.getCart();
      expect(result.length).toEqual(1);
      expect(result[title]).toEqual(jasmine.any(Object));
    });

    it('should remove an item from the cart, and do the same until 0', function () {

      // check cart is empty
      result = cartService.getCart();
      expect(result).toEqual({ 'length': 0 });

      // add items
      cartService.addToCart(mockProducts[0]);
      cartService.addToCart(mockProducts[0]);
      title = mockProducts[0].title;
      result = cartService.getCart();
      expect(result.length).toEqual(1);
      expect(result[title]).toEqual(jasmine.any(Object));

      // remove 1 item
      cartService.removeFromCart(mockProducts[0].title);
      title = mockProducts[0].title;
      result = cartService.getCart();
      expect(result.length).toEqual(1);
      expect(result[title]).toEqual(jasmine.any(Object));

      // remove last item
      cartService.removeFromCart(mockProducts[0].title);
      result = cartService.getCart();
      expect(result).toEqual({ 'length': 0 });
    });

    it('should remove all of an item', function () {
      // check cart is empty
      result = cartService.getCart();
      expect(result).toEqual({ 'length': 0 });

      // add items
      cartService.addToCart(mockProducts[0]);
      cartService.addToCart(mockProducts[0]);
      title = mockProducts[0].title;
      result = cartService.getCart();
      expect(result.length).toEqual(1);
      expect(result[title]).toEqual(jasmine.any(Object));

      // remove all items
      cartService.removeFromCart(mockProducts[0].title, true);
      result = cartService.getCart();
      expect(result).toEqual({ 'length': 0 });
    });

    it('should empty the cart', function () {
      // check cart is empty
      result = cartService.getCart();
      expect(result).toEqual({ 'length': 0 });

      // add items
      cartService.addToCart(mockProducts[0]);
      cartService.addToCart(mockProducts[1]);
      title = mockProducts[0].title;
      title1 = mockProducts[1].title;
      result = cartService.getCart();
      expect(result.length).toEqual(2);
      expect(result[title]).toEqual(jasmine.any(Object));
      expect(result[title1]).toEqual(jasmine.any(Object));

      // empty cart
      cartService.emptyCart();
      result = cartService.getCart();
      expect(result).toEqual({ 'length': 0 });
    });
  });
}());

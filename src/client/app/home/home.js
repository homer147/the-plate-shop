(function () {
  'use strict';

  var app = angular.module('plateShop');

  app.controller('homeCtrl', ['$scope', 'productService', function ($scope, productService) {
    $scope.model = [];

    productService.products().
      then(function (data) {
        $scope.products = data.data;
      });
  }]);
}());

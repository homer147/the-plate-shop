/* jslint node: true */
/* global inject, expect, jasmine, getJSONFixture */

(function () {
  'use strict';

  describe('homeCtrl - home page controller', function () {
    var createController,
      scope,
      homeCtrl;

    beforeEach(function () {
      module('plateShop');
    });

    beforeEach(inject(function ($rootScope, $controller) {

      createController = function () {
        scope = $rootScope.$new();

        return $controller('homeCtrl', {
          $scope: scope
        });
      };

      homeCtrl = createController();
    }));

    it('should set default variables', function () {
      expect(scope.model).toEqual([]);
    });
  });
}());

(function () {
  'use strict';

  var app = angular.module('plateShop');

  app.directive('productTile', ['cartService', function (cartService) {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/app/product/productTile.html',
      link: function (scope) {
        scope.addToCart = function (something) {
          cartService.addToCart(something);
        };
      }
    };
  }]);
}());

(function () {
  'use strict';

  var app = angular.module('plateShop');

  app.service('productService', ['$http', '$q', function ($http, $q) {

    var products,
      deferred;

    products = function () {
      deferred = $q.defer();
      $http.get('/products').then(function (data) {
        deferred.resolve(data);
      });

      return deferred.promise;
    };

    return {
      products: products
    };

  }]);
}());

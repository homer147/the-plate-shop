/* jslint node: true */
/* global inject, expect, jasmine, getJSONFixture */

(function () {
  'use strict';

  describe('productService - service', function () {
    var productService,
      httpBackend,
      mockProducts;

    beforeEach(function () {
      module('plateShop');
    });

    beforeEach(module(function($urlRouterProvider) {
      $urlRouterProvider.deferIntercept();
    }));

    beforeEach(inject(function (_productService_, _$httpBackend_) {
      productService = _productService_;
      httpBackend = _$httpBackend_;
      jasmine.getJSONFixtures().fixturesPath = 'base/src/client/app/product';
      mockProducts = getJSONFixture('productService.spec.json');
    }));

    it('should be defined', function () {
      expect(productService).toBeDefined();
    });

    it('should get the products from the api', function (done) {
      httpBackend.when('GET', '/products').respond(200, mockProducts);

      productService.products().then(function(data) {
        expect(data.data).toEqual(mockProducts);
        done();
      });

      httpBackend.flush();
    });
  });
}());

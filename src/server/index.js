/* jslint node: true */
'use strict';

const express = require('express');
const app = express();
const fs = require('fs');

app.use(express.static('./'));
app.use(express.static('./src/client/'));
app.get('/products', function (req, res) {
    fs.readFile('json/products.json', 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        res.send(data);
    });
});
app.all('/*', function(req, res) {
  res.sendFile('../../src/client/index.html', { root: __dirname });
});

app.listen(3000, function () {
  console.log('listening on port 3000');
});
